#include<iostream>
#include"MyQueue.h"
using namespace std;
enum class READ_STATUS{
	NUMBER,
	SPACE,
	OTHER
};
int main(){
	Queue_Cir<int> mQueue;
	int number = 0;
	READ_STATUS readState = READ_STATUS::OTHER;
	int num = 0;
	while(num != EOF){
		num = cin.get();
		if(num >= '0' && num <= '9'){
			if(readState == READ_STATUS::NUMBER){
				number = number*10+(num-'0');
			}else{
				number = num - '0';
			}
			readState = READ_STATUS::NUMBER;
		}else{
			if(readState == READ_STATUS::NUMBER){
				mQueue.EnQueue(number);
				number = 0;
			}
			if(num == ' ' || num == '\n' ){
				readState = READ_STATUS::SPACE;
			}else{
				readState = READ_STATUS::OTHER;
			}
		}
	}
	cout<<"\n result: "<<endl;
	while(!mQueue.IsEmpty()){
		int temp;
		mQueue.DeQueue(temp);
		cout<<temp<<" ";
	}


	system("pause");
	return 0;
}